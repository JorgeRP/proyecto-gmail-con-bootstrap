<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Document</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>

	<!--primer contenedor, logo de google y botón crear cuenta -->
	<div class="container-fluid well">
		<div class="row">
			<!--columna logo con imagen responsive-->
			<div class="col-xs-3 col-sm-3 col-sm-offset-1 col-md-2 col-md-offset-2 col-lg-2 col-lg-offset-3">
				<img src="logo.png" class="img-responsive"/>
			</div>
			<!--columna link iniciar sesion solo visible en xs-->
			<div class="col-xs-4 visible-xs">
				<small><a href="#">Iniciar sesión</a></small>
			</div>
			<!--columna botón crear cuenta solo visible en xs-->
			<div class="col-xs-3 visible-xs">
				<button type="button" class="btn btn-primary">Crear una cuenra</button>
			</div>
			<!--columna link y boton solo son visibles en sm md lg-->
			<div class="col-sm-4 col-sm-offset-3 visible-sm visible-md visible-lg col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-3">
				<small><a href="#">Iniciar sesión</a></small>
				<button type="button" class="btn btn-primary">Crear una cuenta</button>
			</div>
		</div>
	</div>
	<!--fin del primer contenedor-->
	<!--segundo contenedor menu secundario-->
	<div class="container-fluid">
		<div class="row" id="menu-sec">
			<!--menu secundario visible en sm lg-->
			<div class="col-sm-12 col-sm-offset-1 visible-sm visible-md col-md-8 col-md-offset-2 visible-lg col-lg-9 col-lg-offset-3">
				<img src="googlemail-16.png" alt=""><strong>Gmail</strong>
				<a href="#" class="enlace-menu2">Funciones</a>
				<a href="#" class="enlace-menu2">Para dispositivos móviles</a>
				<a href="#" class="enlace-menu2">Para empresas</a>
				<a href="#" class="enlace-menu2">Ayuda</a>
				<br><p></p>
			</div>
			<!--menu compacto es visible en xs-->
			<div class="col-xs-12 visible-xs">
				<nav class="navbar navbar-default" role="navigation">
				<div class="container-fluid">
			
					<!--Brand and toggle get grouped for better mobile display-->
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a href="#" class="navbar-brand"><img src="googlemail-16.png" alt=""><small><strong>Gmail</strong></small></a>
					</div>

			<!--collect the nav links, forms, and other content for toggling-->
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						<ul class="nav navbar-nav">
							<li>
								<a href="#" class="enlace-menu">Funciones</a>
							</li>
							<li>
								<a href="#" class="enlace-menu">Para dispositivos móviles</a>
							</li>
							<li>
								<a href="#" class="enlace-menu">Para empresas</a>
							</li>
							<li>
								<a href="#" class="enlace-menu">Ayuda</a>
							</li>
						</ul>

					</div>
				</div>
				</nav>
			</div>
		</div>
	</div>
	<!-- fin del segundo contenedor-->
	<!--Tercer contenedor carrusel imágenes-->
	<div class="container" id="sha">
		<div class="row">
			
			<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

				<!--indicators-->
				<ol class="carousel-indicators">
					<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
					<li data-target="#carousel-example-generic" data-slide-to="1"></li>
					<li data-target="#carousel-example-generic" data-slide-to="2"></li>
				</ol>

				<div class="carousel-inner">
					<div class="item active">
						<img src="about-carousel-1.png" alt="Gmail en todo tipo de dispositivos">
						<div class="carousel-caption">
							<h4>La sencillez y facilidad de Gmail en todo tipo de dispositivos</h4>
							<p><button type="button" class="btn btn-primary">Crear una Cuenta</button></p>
						</div>
					</div>
					<div class="item">
						<img src="about-carousel-2.png" alt="Mensajes por tipo para organizarte mejor">
						<div class="carousel-caption">
							<h4>La bandeja de entrada de Gmail clasifica tus mensajes por tipo para organizarte mejor</h4>
							<p><button type="button" class="btn btn-primary">Crear una Cuenta</button></p>
						</div>
					</div>
					<div class="item">
						<img src="about-carousel-3.jpg" alt="Chatea con un compañero o llama por teléfono">
						<div class="carousel-caption">
							<h4>Habla con amigos en una videollamada, chatea con un compañero o llama por teléfono sin salir de tu bandeja de entrada</h4>
							<p><button type="button" class="btn btn-primary">Crear una Cuenta</button></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--fin del tercer contenedor-->
	<!--Cuarto contenedor contenido-->
	<div class="container" id="sha">
		<div class="row">
			<div class="col-xs-12">
				<br>
				<h2><p class="text-center">Te damos la bienvenida a Gmail</p></h2>
				<br>
			</div>
		</div>
		<!--primer bloque izquierda-->
		<div class="row">
			<!--imagenes responsive-->
			<div class="col-xs-12 col-sm-3">
				<center><img src="mobile-feature-1.png" class="img-responsive"></center>
			</div>
			<!--texto del contenido-->
			<div class="col-xs-12 col-sm-3">
				<br>
				<strong>Disfruta de Gmail en cualquier dispositivo</strong>
				<small><p>Tienes lo mejor de Gmail estés donde estés y en cualquier dispositivo
				<a href="#">Más información sobre Gmail para móviles</a></p></small>
			</div>

			<!--imagen responsive-->
			<div class="col-xs-12 col-sm-3">
				<center><img src="circle-hangout-video.png" class="img-responsive"></center>
			</div>
			<!--texto del contenido-->
			<div class="col-xs-12 col-sm-3">
				<br>
				<strong>Habla en diferentes dispositivos</strong>
				<small><p>Hangouts da vida a la conversación con fotos, emojis e incluso videollamadas de grupo gratis. Habla... <a href="#">Más información sobre Hangouts</a></p></small>
			</div>
		</div>
		<!--segundo bloque derecha-->
		<div class="row">
			<!--imagen responsive-->
			<div class="col-xs-12 col-sm-3">
				<center><img src="circle-inbox-b.png" width="184" height="184" class="img-responsive"></center>
			</div>
			<!--texto del contenido-->

			<div class="col-xs-12 col-sm-3">
				<br>
				<strong>Descubre la nueva bandeja de entrada</strong>
				<small><p>Con las nuevas pestañas personalizables lo controlados todo. <a href="#">Más información sobre la nueva bandeja de entrada</a></p></small>
			</div>

			<!--imagen responsive-->
			<div class="col-xs-12 col-sm-3">
				<center><img src="circle-storage.png" class="img-responsive"></center>
			</div>
			<!--texto del contenido-->

			<div class="col-xs-12 col-sm-3">
				<br>
				<strong>Un montón de espacio gratis</strong>
				<small><p>Se acabó lo de borrar mensajes para ahorrar espacio.<a href="#">Más información sobre el almacenamiento</a></p></small>
			</div>
		</div>
	</div>
	<!--fin del cuarto contenedor-->
	<!--quinto contenedor pie de página-->

	<div class="container-fluid well">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-9 col-lg-offset-3">
				<br>

				<small class="pull-left">
					<a href="#" class="enlace-sesion">Nuestra política</a>
					<a href="#" class="enlace-sesion">Ayuda</a>
					<a href="#" class="enlace-sesion">Para empresas</a>
					<a href="#" class="enlace-sesion">Para dispositivos móviles</a>
				</small>

				<a href="#" class="pull-left"><img src="gplus.jpg" class="img-responsive"></a>
				<br>
			</div>
		</div>
	</div>

	<!--fin del quinto contenedor-->
	<!--sexto contenedor-->
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-5 col-sm-push-7 col-md-3 col-md-push-9 col-lg-3 col-lg-push-8">
				<!--Poner la imagen al lado del elemto como box-->
				<a class="pull-left" href="#">
					<img class="media-object" src="h1.png" class="img-responsive" alt="Idioma">
				</a>
				<!--combo box con lista de idiomas-->
				<select onchange="var href=this[this.selectedIndex].value;if(href=!''){window.location.href=href};">
					<option value="/intl/ms/mail/help/about.html">Castellano</option>
					<option value="/intl/ms/mail/help/about.html">Català</option>
					<option value="/intl/ms/mail/help/about.html">Galego</option>
					<option value="/intl/ms/mail/help/about.html">Euskera</option>
				</select>
			</div>
			<!--Enlaces finales combinacion de clases push y pull-->
			<div class="col-xs-12 col-sm-5 col-sm-pull-5 col-md-5 col-md-pull-3 col-lg-4 col-lg-pull-2">
				<small><a href="#" class="menu-footer">Google - </a>
					<a href="#" class="menu-footer">Acerca de Google - </a>
					<a href="#" class="menu-footer">Privacidad y Condiciones</a>
				</small>
				<br><br>
			</div>
		</div>
	</div>
	<!--fin del sexto contenedor-->






	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
</body>
</html>